Project page: www.fleetco.space

System requirements:

-Web Hosting (XAMPP/WAMP can also be used)
-PHP 4+
-MySQL Database


How to install

1. Unzip the file
2. Upload the file to the hosting root or folder 
3. Create a MySQL database using phpmyadmin or otherwise
4. In the unzip folder, locate the SQL dump file called mysql-dump-with dummy-data.sql.
5. Import this SQL dump to the database created (This database contains some sample data which you can delete later)
6. Enter the database connectivity details as follows:

Find "ConnectionManager.php" in connections folder and on line 254-258 find below block of code:

		$data["connInfo"][0] = "database_host_name";
		$data["connInfo"][1] = "database_user_name";
		$data["connInfo"][2] = "database_user_password";
		$data["connInfo"][3] = "database_port_number";
		$data["connInfo"][4] = "database_name";

Update your database connectivity details above. 

Default values for database port & host name as follows:

database_host_name = localhost
database_port_number = 3306

If above default details do not work, contact your hosting provider for the same.

7. Then visit the url and the login screen should appear now.
8. Use following admin logins:

Username: Admin
Password: AdminF123

Once you log in, go to Admin Area. You can create any no of users with various user rights to different tables.